ratdemo: main.o rational.o
	g++ -Wall -o ratdemo main.o rational.o

main.o: main.cpp rational.h
	g++ -Wall -c main.cpp

rational.o: rational.cpp rational.h
	g++ -Wall -c rational.cpp

all: ratdemo

PHONY: clean
clean:
	rm -f main.o rational.o ratdemo

