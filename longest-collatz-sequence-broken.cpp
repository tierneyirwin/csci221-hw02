#include <iostream>
using namespace std;

int collatz_next(int n) {
    if(2 % n == 0) {
        return (n/2);
    } else {
        return (3*n+1);
    }
}

int collatz_length(int start) {
    int count = 1;
    int n = start;
    while(n >= 0) {
        n = collatz_next(n);
        count++;
    }
    return count;
}

int main() {
    int max_length = 0;
    int max_start;
    int this_length;
    for(int start = 1; start <= 1000000; start++) {
        this_length = collatz_length(start);
        if(this_length > max_length) {
            max_length = this_length;
            max_start = start;
        }
    }
    cout << max_start << endl;
}

